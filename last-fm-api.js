// import the modules
var request = require("request") 

// last.fm api information
var last_fm_api_url = 'http://ws.audioscrobbler.com/2.0/' 
var last_fm_api_key = 'd5adeaee26ea9f9621f4be79ce14950b' 

// query track from last.fm api
exports.queryTracks = function(parameters, callback) {
    var resultTracks = [] 

    var querys = {
        method: 'track.search',
        track: parameters.track,
        api_key: last_fm_api_key,
        format: 'json'
    } 

    if (parameters.artist != '')
        querys.artist = parameters.artist 

    request.get({
            url: last_fm_api_url,
            qs: querys
        },
        function(error, response, body) {
            var result = JSON.parse(body) 
            var tracks = result.results.trackmatches.track 
            for (var i = 0;  i < tracks.length;  i++) {
                resultTracks.push({
                    track: tracks[i].name,
                    artist: tracks[i].artist,
                    image: tracks[i].image[2]["#text"]
                }) 
            }
            callback(resultTracks) 
        }) 
}