// import the modules/part3_client/index.html
var request = require("request");
var restify = require("restify");
var url = require("url");
var util = require("util");
var queryTracks = require("./last-fm-api").queryTracks; 
var queryLyrics = require("./musixmatch-api").queryLyrics;
var register = require("./firebase-api").register;
var login = require("./firebase-api").login;
var addToFavorites = require("./firebase-api").addToFavorites;
var queryFavorites = require("./firebase-api").queryFavorites;
var delFavorites = require("./firebase-api").deleteFavorites;

// server setting
var server = restify.createServer(); 

// server module
server.use(restify.fullResponse()).use(restify.bodyParser()).use(restify.CORS({
    credentials: true
})); 

// route setting
server.get("/tracks", getTracks); 
server.get("/lyrics", getLyrics); 
server.get("/getfavorites", getPrintFavorites);
server.post("/login", postLogin);
server.post("/favorites", postFavorites);
server.post("/register", postRegister);
server.del("/deletefavorites", deleteFavorites);


// server running port
var port = process.env.PORT || 8080; 
server.listen(port, function(error) {
    if (error) {
        console.error(error); 
    }
    else {
        console.log('App is ready at : ' + port); 
    }
});

// "/tracks" route handler
function getTracks(request, response) {
    // get url parameters as object
    var parameters = url.parse(request.url, true).query; 

    queryTracks(parameters, printTracks); 

    function printTracks(tracks) {
        var trackmatches = {
            tracks: tracks
        }; 
        response.setHeader('Content-type', 'application/json'); 
        response.charSet('utf-8'); 
        if (util.isNullOrUndefined(tracks)) {
            response.send(200, {
                tracks: ""
            });
        }
        else {
            response.send(200, trackmatches);
        }
        response.end();
    }
}

// "/lyrics" route handler
function getLyrics(request, response) {
    // get url parameters as object
    var parameters = url.parse(request.url, true).query;

    queryLyrics(parameters, printLyrics); 

    function printLyrics(lyrics) {
        var lyricsmatches = {
            lyrics: lyrics
        }; 
        response.setHeader('Content-type', 'application/json');
        response.charSet('utf-8');
        if (util.isNullOrUndefined(lyrics)) {
            response.send(200, {
                lyrics: ""
            });
        }
        else {
            response.send(200, lyricsmatches);
        }
        response.end();
    }
}

// "/login" route handler
function postLogin(request, response) {
    var parameters = request.params;
    
    login(parameters, responseMessage);
    
    function responseMessage(message) {
        response.setHeader('Content-type', 'application/json') ;
        response.charSet('utf-8');
        response.send(200, message);
        response.end();
    }
}

// "/register" route handler
function postRegister(request, response) {
    var parameters = request.params;
    
    register(parameters, responseMessage);
    
    function responseMessage(message) {
        response.setHeader('Content-type', 'application/json');
        response.charSet('utf-8');
        response.send(200, message);
        response.end();
    }
}

// "/favorites" route handler
function postFavorites(request, response) {
    var parameters = request.params;
    
    addToFavorites(parameters, responseMessage);
    
    function responseMessage(message) {
        response.setHeader('Content-type', 'application/json');
        response.charSet('utf-8');
        response.send(200, message);
        response.end();
    }
}

// "/getfavorites" route handler
function getPrintFavorites(request, response) {
    var parameters = url.parse(request.url, true).query; 
    
    queryFavorites(parameters, responseMessage);
    
    function responseMessage(message) {
        response.setHeader('Content-type', 'application/json');
        response.charSet('utf-8');
        response.send(200, message);
        response.end();
    }
}

// "/deletedavorites" route handler
function deleteFavorites(request, response) {
    var parameters = url.parse(request.url, true).query;
    
    delFavorites(parameters, responseMessage);
    
    function responseMessage(message) {
        response.setHeader('Content-type', 'application/json');
        response.charSet('utf-8');
        response.send(200, message);
        response.end();
    }
}