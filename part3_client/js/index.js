/* global angular */

var app = angular.module('app', ['ngRoute', 'ngCookies', 'ngAnimate']);

app.config(function($routeProvider) {
    $routeProvider
        .when('/search', {
            templateUrl: 'templates/search_result.html',
            controller: 'printTracks'
        })
        .when('/lyrics/:track/:artist', {
            templateUrl: 'templates/lyrics_review.html',
            controller: 'lyricsReviews'
        })
        .when('/favorites/:userid', {
            templateUrl: 'templates/favorites.html',
            controller: 'printFavorties'
        })
        .otherwise({
            redirectTo: '/search'
        });
});

app.factory('SharedData', function() {
    var share = {
        tracks: ""
    };
    
    var image = "";
    
    return {
        setTracks: function(tracks) {
            share.tracks = tracks;
        },
        setImage: function(img) {
            image = img;
        },
        getTracks: function() {
            return share;
        },
        getImage: function() {
            return image;
        }
    };
});

app.directive('confirmationNeeded', function($cookies, $window) {
    return {
        priority: 1,
        terminal: true,
        link: function(scope, element, attr) {
            var msg = attr.confirmationNeeded || "Are you sure to Logout?";
            var clickAction = attr.ngClick;
            element.bind('click', function() {
                if (window.confirm(msg)) {
                    scope.$eval(clickAction);
                    $cookies.remove('loggedIn');
                    $cookies.remove('uid');
                    $window.location.href = "index.html";
                }
            });
        }
    };
});

app.controller('registerHandler', function($scope, $http, $window, $cookies, $timeout) {
    $scope.register = function() {
        var data = {
            username: $scope.username,
            password: $scope.password1,
            email: $scope.email
        };
        
        $http.post("https://assignmentpart2and3.herokuapp.com/register", data)
            .then(function(response) {
                if (response.data.success) {
                    $scope.registered = true;
                    $timeout(function() {
                        $window.location.href = "login.html";
                    }, 2000);
                }
                else if (response.data.error == "username duplicated") {
                    $scope.duplicate = true;
                }
            }, function(error) {
                console.log(error);
            });
    };
    
    $scope.reset = function() {
        $scope.username = '';
        $scope.password1 = '';
        $scope.password2 = '';
        $scope.email = '';
    };
});

app.controller('loginHandler', function($scope, $http, $cookies, $window, $timeout, SharedData) {
    var loggedIn = $cookies.get('loggedIn') === undefined ? true : false;

    $scope.loginStatus = loggedIn;
    
    if (!loggedIn) {
        $scope.username = $cookies.get('loggedIn');
        $scope.userid = $cookies.get('uid');
    }
    
    $scope.login = function() {
        var data = {
            username: $scope.username,
            password: $scope.password
        };
        
        $http.post("https://assignmentpart2and3.herokuapp.com/login", data)
            .then(function(response) {
                if (response.data.success) {
                    $scope.loggedInFail = false;
                    $scope.loggedIn = true;
                    $cookies.put('loggedIn', response.data.success.username);
                    $cookies.put('uid', response.data.userid);
                    $timeout(function() {
                        $window.location.href = "index.html";
                    }, 2000);
                }
                else if (response.data.error) {
                    $scope.loggedIn = false;
                    $scope.loggedInFail = true;
                }
            }, function(error) {
                console.log(error);
            });
    };
    
    $scope.logout = function() {
        // $cookies.remove('loggedIn');
        // $window.location.href = "index.html";
    };
});

app.controller('searchTracks', function($scope, $http, SharedData) {
    $scope.submit = function() {
        var track = $scope.track;
        var artist = $scope.artist === undefined ? '' : $scope.artist;
        
        $scope.loading = true;
        
        $http.get('https://assignmentpart2and3.herokuapp.com/tracks?track=' + track + '&artist=' + artist)
            .then(function(response) {
                if (response.status != 400) {
                    SharedData.setTracks(response.data.tracks);
                }
            }).finally(function() {
                $scope.loading = false;
            });
    };
});

app.controller('printTracks', function($scope, $http, $cookies, SharedData) {
    $scope.$watch(function() {
        return SharedData.getTracks().tracks;
    }, function(newVal, oldVal) {
        $scope.tracks = newVal;
    });
    
    $scope.setImage = function(image) {
        SharedData.setImage(image);
    };
    
     
    $scope.addToFavorites = function(trackInfo) {
        var data = {
            track: trackInfo,
            userid: $cookies.get('uid'),
        };
        
        if ($cookies.get('loggedIn') && $cookies.get('uid')) {
            $http.post("https://assignmentpart2and3.herokuapp.com/favorites", data)
                .then(function(response) {
                    if (response.data.success) {
                        window.alert(data.track.track + " - " + data.track.artist + ", is added to your favorites!");
                    }
                    else {
                        window.alert(data.track.track + " - " + data.track.artist + ", you were already added!\nPlease add others.");
                    }
                }, function(error) {
                    console.log(error);
                });
        }
        else {
            window.alert("Please login to continue to use this function.");
        }
    };
});

app.controller('lyricsReviews', function($scope, $http, $cookies, $routeParams, SharedData) {
    var track = $routeParams.track;
    var artist = $routeParams.artist;
    
    $scope.track = track;
    $scope.artist = artist;
    $scope.image = SharedData.getImage();
    $scope.userid = $cookies.get('uid');

    $http.get('https://assignmentpart2and3.herokuapp.com/lyrics?track=' + track + '&artist=' + artist)
        .then(function(response) {
            if (response.status != 400) {
                $scope.lyrics = response.data.lyrics;
            }
            else {
                $scope.lyrics = 'Sorry. Lyrics not found.';
            }
        });
});

app.controller('printFavorties', function($scope, $http, $routeParams) {
    var userid = $routeParams.userid;
    
    var scope = angular.element(document.querySelector('[ng-controller=searchTracks]')).scope();
    scope.loading = true;
    
    $http.get('https://assignmentpart2and3.herokuapp.com/getfavorites?uid=' + userid)
        .then(function(response) {
            if (response.data.success) {
                $scope.favorites = response.data.success;
            }
        }, function(error) {
            console.log(error);
        }).finally(function() {
            scope.loading = false;
        });
        
    $scope.deleteFavorites = function($event, id, track, artist) {
        var currentTr = $event.currentTarget.parentNode.parentNode;
        
        if (window.confirm("Are you sure to delete " + track + " - " + artist + "?")) {
            $http.delete('https://assignmentpart2and3.herokuapp.com/deletefavorites?uid=' + userid + "&fid=" + id)
                .then(function(response) {
                    if (response.data.success) {
                       currentTr.remove();
                    }
                }, function(error) {
                    console.log(error);
                });
        }
    };
})