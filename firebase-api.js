// import the modules
var firebase = require("firebase");

// initialize the firebase
firebase.initializeApp({
    serviceAccount: "assignment-part2-part3-950bbab3b1f1.json",
    databaseURL: "https://assignment-part2-part3.firebaseio.com/",
});

exports.register = function(parameters, callback) {
    var ref = firebase.database().ref('user/');
    var duplicate = false;
    
    ref.orderByChild('username').equalTo(parameters.username).once('value', function(data) {
        if (data.val() == null) {
            var userRef = ref.push(parameters, function(error) {
                if (error) {
                    callback({error: "register push firebase error"});
                }
                else {
                    callback({success: userRef.toString()});
                }
            });
        }
        else {
            callback({error: "username duplicated"});
        }
    });
};

exports.login = function(parameters, callback) {
    var ref = firebase.database().ref('user/');

    ref.orderByChild('username').equalTo(parameters.username).once('value', function(data) {
        data.forEach(function(user) {
            if (user.val().password == parameters.password) {
                callback({success: user.val(), userid: user.key});
            }
            else {
                callback({error: "register first"});         
            } 
        });
    });
};

exports.addToFavorites = function(parameters, callback) {
    var ref = firebase.database().ref('user/' + parameters.userid + "/favorites");
    var duplicate = false;
    
    ref.orderByChild('track').equalTo(parameters.track.track).once('value', function(data) {
        if (data.val() == null) {
            var favoritesRef = ref.push({
                track: parameters.track.track,
                artist: parameters.track.artist,
                image: parameters.track.image
            }, function(error) {
                if (error) {
                    callback({error: "favorites push firebase error"});
                }
                else {
                    callback({success: favoritesRef.toString()});
                }
            });
        }
        else {
            data.forEach(function(trackInfo) {
                if ((trackInfo.val().track == parameters.track.track) && (trackInfo.val().artist == parameters.track.artist)) {
                    duplicate = true;
                    callback({error: "favorites were already added"});
                    return duplicate;
                }
            });
            
            if (!duplicate) {
                var favoritesRef = ref.push({
                    track: parameters.track.track,
                    artist: parameters.track.artist,
                    image: parameters.track.image
                }, function(error) {
                    if (error) {
                        callback({error: "favorites push firebase error"});
                    }
                    else {
                        callback({success: favoritesRef.toString()});
                    }
                });
            }
        }
    });
};

exports.queryFavorites = function(parameters, callback) {
    var favorites = [];
    var ref = firebase.database().ref('user/' + parameters.uid + '/favorites');
    
    ref.once('value', function(data) {
        data.forEach(function(track) {
            favorites.push({
                id: track.key,
                track: track.val().track,
                artist: track.val().artist,
                image: track.val().image
            });
        });
        callback({success: favorites});
    });
};

exports.deleteFavorites = function(parameters, callback) {
    var ref = firebase.database().ref('user/' + parameters.uid + '/favorites/' + parameters.fid);
    
    var deleteRef = ref.remove(function(error) {
        if (error) {
            callback({error: "remove error"});
        }
        else {
            callback({success: deleteRef.toString()});
        }
    });
}