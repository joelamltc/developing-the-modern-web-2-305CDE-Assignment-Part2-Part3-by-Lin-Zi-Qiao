// import the modules
var request = require("request") 

// musicmatch api information
var musixmatch_api_url = 'http://api.musixmatch.com/ws/1.1/matcher.lyrics.get' 
var musixmatch_api_key = 'd8a8cc1c7da1505e7552ec6e56e66ee7' 

// query lyrics from musixmatch api
exports.queryLyrics = function(parameters, callback) {
    var querys = {
        apikey: musixmatch_api_key,
        format: 'json',
        q_track: parameters.track
    } 

    if (parameters.artist != '')
        querys.q_artist = parameters.artist 
    
    request.get({
            url: musixmatch_api_url,
            qs: querys
        },
        function(error, response, body) {
            var result = JSON.parse(body) 
            var lyrics 
            if (result.message.header.status_code == "200") {
                lyrics = result.message.body.lyrics.lyrics_body 
            }
            else {
                lyrics = null 
            }
            
            callback(lyrics) 
        }) 
}